<?php

namespace App\Api\V1\Controllers;

use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Events\MailgunWebhook;

class MailgunController extends Controller
{
    /**
     * Store a new document.
     *
     * @return \Illuminate\Http\JsonResponse
     */
     public function store(Request $request, $id)
     {
        $data = $request->all();
        event(new MailgunWebhook($data, $id));

        return response()->json(['status' => 'ok'], 200);
     }
}
