<?php

namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Api\V1\Requests\CollectionCreateRequest;
use App\Api\V1\Requests\CollectionUpdateRequest;
use App\Repositories\CollectionRepository;
use App\Validators\CollectionValidator;
use App\Http\Controllers\Controller;
use App\Entities\Collection;
use Plank\Mediable\Media;

/**
 * Class CollectionsController.
 *
 * @package namespace App\Api\V1\Controllers;
 */
class CollectionsController extends Controller
{
    /**
     * @var CollectionRepository
     */
    protected $repository;

    /**
     * @var CollectionValidator
     */
    protected $validator;

    /**
     * CollectionsController constructor.
     *
     * @param CollectionRepository $repository
     * @param CollectionValidator $validator
     */
    public function __construct(CollectionRepository $repository, CollectionValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $collections = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json($collections);
        }

        return view('collections.index', compact('collections'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CollectionCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(CollectionCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $input = $request->all();
            $collection = $this->repository->create(['name' => null]);
            $resp = $collection->toArray();
            if ($request->has('media')) {
                $collection->attachMedia($input['documents'], 'default');
                $collection->getMedia();
                $resp['media'] = $collection->map(function($file){
                    $newFile = $file->toArray();
                    $newFile['url'] = $file->getUrl();
                    return $newFile;
                });
            }

            if ($request->wantsJson()) {
                return response()->json($resp);
            }

        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $collection = $this->repository->find($id);
        $collection->with('media.document_type')->get();
        $resp = $collection->toArray();
        $resp['media'] = $collection->getMedia('default')->map(function($file){
            $newFile = $file->toArray();
            $newFile['document_type'] = $file->document_type;
            $newFile['url'] = $file->getUrl();
            return $newFile;
        });

        if (request()->wantsJson()) {
            return response()->json($resp);
        }

        return view('collections.show', compact('collection'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $collection = $this->repository->find($id);

        return view('collections.edit', compact('collection'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CollectionUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(CollectionUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $collection = $this->repository->update($request->except('media'), $id);
            $docs = $request->get('media');

            $response = $collection->toArray();

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    public function attach(CollectionUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $collection = $this->repository->find($id);
            $input = $request->all();
            $collection->attachMedia($input['media'], 'default');
            $collection->getMedia('default');

            $resp = $collection->toArray();
            $resp['media'] = $collection->media->map(function($file){
                $newFile = $file->toArray();
                $newFile['url'] = $file->getUrl();
                return $newFile;
            });

            if ($request->wantsJson()) {

                return response()->json($resp);
            }

        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    public function detach(CollectionUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $collection = $this->repository->find($id);
            $input = $request->all();
            $collection->detachMedia($input['media']);
            $collection->getMedia('default');

            $resp = $collection->toArray();

            if ($request->wantsJson()) {

                return response()->json($resp);
            }


        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json($deleted);
        }

        return redirect()->back()->with('message', 'Collection deleted.');
    }

}
