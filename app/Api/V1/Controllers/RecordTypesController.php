<?php

namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Api\V1\Requests\RecordTypeCreateRequest;
use App\Api\V1\Requests\RecordTypeUpdateRequest;
use App\Repositories\RecordTypeRepository;
use App\Validators\RecordTypeValidator;
use App\Http\Controllers\Controller;

/**
 * Class RecordTypesController.
 *
 * @package namespace App\Api\V1\Controllers;
 */
class RecordTypesController extends Controller
{
    /**
     * @var RecordTypeRepository
     */
    protected $repository;

    /**
     * @var RecordTypeValidator
     */
    protected $validator;

    /**
     * RecordTypesController constructor.
     *
     * @param RecordTypeRepository $repository
     * @param RecordTypeValidator $validator
     */
    public function __construct(RecordTypeRepository $repository, RecordTypeValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $recordTypes = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json($recordTypes);
        }

        return view('recordTypes.index', compact('recordTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  RecordTypeCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(RecordTypeCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $recordType = $this->repository->create($request->all());

            $response = $recordType->toArray();

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $recordType = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json($recordType);
        }

        return view('recordTypes.show', compact('recordType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  RecordTypeUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(RecordTypeUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $recordType = $this->repository->update($request->all(), $id);

            $response = $recordType->toArray();

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'RecordType deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'RecordType deleted.');
    }
}
