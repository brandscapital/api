<?php

namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Api\V1\Requests\SnoozeCreateRequest;
use App\Validators\SnoozeValidator;
use App\Http\Controllers\Controller;
use App\Notifications\SnoozeTask;
use App\Entities\TaskType;
use App\Entities\Thread;

/**
 * Class SnoozeController.
 *
 * @package namespace App\Api\V1\Controllers;
 */
class SnoozeController extends Controller
{

    /**
     * @var SnoozeValidator
     */
    protected $validator;

    /**
     * SnoozesController constructor.
     *
     * @param SnoozeValidator $validator
     */
    public function __construct(SnoozeValidator $validator)
    {
        $this->validator  = $validator;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  SnoozeCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(SnoozeCreateRequest $request)
    {
        try {
          
            $input = $request->all();
            $this->validator->with($input)->passesOrFail(ValidatorInterface::RULE_CREATE);
            $taskType = TaskType::findOrFail($input['task_type_id']);
            $thread = Thread::findOrFail($input['thread_id']);
            $snooze = $request->user()
              ->notify((new SnoozeTask($taskType, $thread))->delay(now()->addMinutes($input['minutes'])));
              
            if ($request->wantsJson()) {
                return response()->json([]);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }
}
