<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Plank\Mediable\Mediable;
use Spatie\Activitylog\Traits\LogsActivity;
use Prettus\Repository\Traits\PresentableTrait;

/**
 * Class Process.
 *
 * @package namespace App\Entities;
 */
class Process extends Model implements Transformable
{
    use TransformableTrait;
    use Mediable;
    use LogsActivity;
    use PresentableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['process_instance_id', 'workflow_id'];

    public function workflows()
    {
        return $this->belongsToMany('App\Entities\Workflow');
    }

    public function presenter()
    {
        return "App\\Presenter\\ProcessPresenter";
    }

}
