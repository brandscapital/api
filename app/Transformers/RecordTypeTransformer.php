<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\RecordType;

/**
 * Class RecordTypeTransformer.
 *
 * @package namespace App\Transformers;
 */
class RecordTypeTransformer extends TransformerAbstract
{
    /**
     * Transform the RecordType entity.
     *
     * @param \App\Entities\RecordType $model
     *
     * @return array
     */
    public function transform(RecordType $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
