<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\DocumentType;

/**
 * Class TypeTransformer.
 *
 * @package namespace App\Transformers;
 */
class TypeTransformer extends TransformerAbstract
{
    /**
     * Transform the DocumentType entity.
     *
     * @param \App\Entities\DocumentType $model
     *
     * @return array
     */
    public function transform(DocumentType $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
