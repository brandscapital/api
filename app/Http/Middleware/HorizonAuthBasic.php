<?php

namespace App\Http\Middleware;

use Closure;

class HorizonAuthBasic
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle($request, Closure $next, $guard = null)
     {
         return $this->auth->guard($guard)->basic('email') ?: $next($request);
     }
}
