<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;
use App\Entities\TaskType;
use App\Entities\Thread;

class SnoozeTask extends Notification implements ShouldQueue
{
    use Queueable;
    protected $user, $taskType, $thread;
    
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(TaskType $taskType, Thread $thread)
    {
      $this->taskType = $taskType;
      $this->thread = $thread;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }
    
    /**
     * Get the broadcastable representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
      return new BroadcastMessage([
        'thread_id' => $this->thread->id,
        'message' => "Snooze Task: Reminder to complete {$this->taskType->name}"
      ]);
    }
    
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
          'thread_id' => $this->thread->id,
          'message' => "Snooze Task: Reminder to complete {$this->taskType->name}"
        ];
    }
}
