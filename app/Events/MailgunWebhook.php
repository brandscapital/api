<?php

namespace App\Events;

use App\Entities\Thread;

class MailgunWebhook
{
    public $data;
    public $workflowId;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($data, $workflowId)
    {
        $this->data = $data;
        $this->workflowId = $workflowId;
    }
}
