<?php

namespace App\Listeners;
use App\Camunda\Entity\Request\ProcessDefinitionRequest;
use App\Camunda\Service\ProcessDefinitionService;
use App\Camunda\Helper\VariableCollection;
use App\Entities\Thread;
use Carbon\Carbon;

class MailgunEventSubscriber
{

    public function __construct() {
        $this->service = new ProcessDefinitionService(config('services.camunda.host'));
    }

    /**
    * Handle user create events.
    */
    public function onMailgunWebhook($event) {
        $data = $event->data;
        $workflowId = $event->workflowId;
        $vars = new VariableCollection();
        $vars->addVariable('from', $data['from']);
        $vars->addVariable('content', $data['body-plain']);
        $vars->addVariable('date', Carbon::createFromTimestamp($data['timestamp'])->toDateTimeString());
        $req = new ProcessDefinitionRequest();
        $req->set('variables', $vars);
        $res = $this->service->startInstanceByKey($workflowId, null, $req);
    }

    /**
    * Register the listeners for the subscriber.
    *
    * @param  \Illuminate\Events\Dispatcher  $events
    */
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\MailgunWebhook',
            'App\Listeners\MailgunEventSubscriber@onMailgunWebhook'
        );
    }
}
