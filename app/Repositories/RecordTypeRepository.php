<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RecordTypeRepository.
 *
 * @package namespace App\Repositories;
 */
interface RecordTypeRepository extends RepositoryInterface
{
    //
}
