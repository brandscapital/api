<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CollectionRepository.
 *
 * @package namespace App\Repositories;
 */
interface CollectionRepository extends RepositoryInterface
{
    //
}
