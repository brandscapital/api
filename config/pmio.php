<?php

return [
  'endpoint' => env('PMIO_ENDPOINT'),
  'access_token' => env('PMIO_ACCESS_TOKEN'),
  'debug' => env('PMIO_DEBUG'),
  'debug_file' => env('PMIO_DEBUG_FILE'),
];
