## Installation

1. run `composer update`;
2. Use Laravel Homestead as a development environment.
3. Configure .env file for laravel project.

Once the project creation procedure will be completed, run the `php artisan migrate` command to install the required tables.

## Tests

In order to run tests:

* create a `homestead_test` database on your machine;
* run `vendor/bin/phpunit`;