<?php

use Illuminate\Database\Seeder;
use App\Entities\User;
use App\Events\UserCreated;

class SeedUsers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userInfo = [
          "first_name" => "Tommy",
          "last_name" => "Cresine",
          "email" => "tcresine@gmail.com",
          "password" => "murakami"
        ];
        $user = User::create($userInfo);
        $user->assignRole('admin');

        $userInfo1 = [
          "first_name" => "Marc",
          "last_name" => "Brands",
          "email" => "test@brandscapital.com",
          "password" => "password"
        ];
        $user1 = User::create($userInfo1);
        $user1->assignRole('admin');
        if (App::environment('stage')) {
            event(new UserCreated($user));
            event(new UserCreated($user1));
        }
    }
}
